#include "pch.h"
#include <iostream>
#include "mpi.h"
#include <Windows.h>

using namespace std;

int main(int argc, char **argv)
{
	MPI_Init(&argc, &argv);

	int rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	int size;
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	cout << "rank  = " << rank << "\tsize = " << size << endl;

	MPI_Finalize();
}

