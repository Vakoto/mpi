#include "pch.h"
#include <iostream>
#include "mpi.h"

using namespace std;

const int previousFactorialTag = 1;
const int additionTag = 2;

static int isOdd(int x)
{
	return x & 1;
}

void sendPreviousFactorial(int rank, uint64_t fact, const int sender, const int lastRecipient)
{
	MPI_Request* requests = new MPI_Request[lastRecipient];
	for (int recipient = 0; recipient <= lastRecipient; recipient++)
	{
		if (recipient == sender)
		{
			continue;
		}
		MPI_Isend(&fact, 1, MPI_INT64_T, recipient, previousFactorialTag, MPI_COMM_WORLD, &requests[recipient]);
	}
	MPI_Waitall(lastRecipient, requests, MPI_STATUSES_IGNORE);
}

uint64_t recvPreviousFactorial(int rank, const int sender)
{
	uint64_t prev_fact;
	MPI_Recv(&prev_fact, 1, MPI_INT64_T, sender, previousFactorialTag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	return prev_fact;
}

bool tryRecvAddition(uint64_t* recvbuf, int step, int rank, MPI_Request* request)
{
	switch (step)
	{
	case 0:
		if (rank == 1)
		{
			return false;
		}
		break;
	case 1:
		if (rank == 2 || rank == 3)
		{
			return false;
		}
		break;
	case 2:
		if (rank >= 4)
		{
			return false;
		}
		break;
	default:
		return false;
	}
	MPI_Irecv(recvbuf, 1, MPI_INT64_T, MPI_ANY_SOURCE, additionTag, MPI_COMM_WORLD, request);
	return true;
}

void sendAddition(int step, int rank, uint64_t k1, uint64_t ans[])
{
	int dest;
	uint64_t buf;
	bool isOddRank;
	switch (step)
	{
	case 0:
		isOddRank = isOdd(rank);
		dest = rank + (isOddRank ? -1 : +1);
		buf = isOddRank ? k1 : ans[0];
		MPI_Send(&buf, 1, MPI_INT64_T, dest, additionTag, MPI_COMM_WORLD);
		break;
	case 1:
		if (rank == 2)
		{
			MPI_Send(&k1,     1, MPI_INT64_T, 0, additionTag, MPI_COMM_WORLD);
			MPI_Send(&ans[0], 1, MPI_INT64_T, 1, additionTag, MPI_COMM_WORLD);
		}
		else if (rank == 5)
		{
			MPI_Send(&ans[1], 1, MPI_INT64_T, 6, additionTag, MPI_COMM_WORLD);
			MPI_Send(&ans[1], 1, MPI_INT64_T, 7, additionTag, MPI_COMM_WORLD);
		}
		else if (rank == 6)
		{																																						k1 *= 12;
			MPI_Send(&k1, 1, MPI_INT64_T, 4, additionTag, MPI_COMM_WORLD);
			MPI_Send(&ans[0], 1, MPI_INT64_T, 5, additionTag, MPI_COMM_WORLD);
		}
		break;
	case 2:
		if (rank == 4)
		{
			MPI_Send(&k1,     1, MPI_INT64_T, 0, additionTag, MPI_COMM_WORLD);
			MPI_Send(&ans[0], 1, MPI_INT64_T, 1, additionTag, MPI_COMM_WORLD);
			MPI_Send(&ans[1], 1, MPI_INT64_T, 2, additionTag, MPI_COMM_WORLD);
		}
		else if (rank == 5)
		{
			MPI_Send(&ans[1], 1, MPI_INT64_T, 3, additionTag, MPI_COMM_WORLD);
		}
		break;
	}
}

void printFactorials()
{
	int rank, size;
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	uint64_t k = rank << 1;
	uint64_t k1 = k + 1;
	uint64_t k2 = k + 2;
	uint64_t fact = k1 * k2;
	uint64_t prev_fact[4];
	uint64_t addition;
	MPI_Request request;
	prev_fact[0] = fact;	
	cout << "rank = " << rank << "\t" << "k1 = " << k1 << "\tk2 = " << k2 << endl;

	MPI_Barrier(MPI_COMM_WORLD);

	if (rank == 0)
	{
		cout << "fact(1) = " << k1 << endl;
		cout << "fact(2) = " << fact << endl;
	}

	for (int i = 0; i < 3; i++)
	{
		const int sender = (1 << i) - 1;
		const int lastRecipient = (1 << i + 1) - 1;
		const int firstAdditionSource = 1 << i;

		bool needWait = tryRecvAddition(&addition, i, rank, &request);

		if (rank >= firstAdditionSource)
		{
			sendAddition(i, rank, k1, prev_fact);
		}

		if (needWait)
		{
			MPI_Wait(&request, MPI_STATUS_IGNORE);
		} 
		else 
		{
			addition = fact;
		}

		if (rank == sender)
		{
			sendPreviousFactorial(rank, fact, sender, lastRecipient);
		} else if (rank <= lastRecipient)
		{
			fact = recvPreviousFactorial(rank, sender);
		} 

		// cout << "step = " << i << "\trank = " << rank << "\tfact = " << fact << "\taddition = " << addition << "\tfa = " << fact * addition << "\tans[i] = " << prev_fact[i] << endl;

		fact *= addition;
		prev_fact[i + 1] = fact;

		if (rank <= lastRecipient)
		{
			cout << "fact(" << (1 << i + 1) + rank + 1 << ") = " << fact << endl;
		}

		MPI_Barrier(MPI_COMM_WORLD);
	}
}

int main(int argc, char* argv[])
{
	MPI_Init(&argc, &argv);
	printFactorials();
	MPI_Finalize();
	return 0;
}