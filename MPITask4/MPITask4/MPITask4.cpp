#include "pch.h"
#include <iostream>
#include "mpi.h"

using namespace std;

int main(int argc, char* argv[])
{
	int rank, size, message = 0;
	MPI_Status status;

	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	for (int i = 0; i < 2; i++)
	{
		if (rank == 0) 
		{
			message++;
			MPI_Send(&message, 1, MPI_INTEGER, rank + 1, 43, MPI_COMM_WORLD);
			MPI_Recv(&message, 1, MPI_INTEGER, size - 1, 43, MPI_COMM_WORLD, &status);
			cout << "rank =  " << rank << "\tmessage = " << message << endl;
		}
		else if (rank == size - 1) 
		{
			MPI_Recv(&message, 1, MPI_INTEGER, rank - 1, 43, MPI_COMM_WORLD, &status);
			cout << "rank =  " << rank << "\tmessage = " << message << endl;
			message++;
			MPI_Send(&message, 1, MPI_INTEGER, 0, 43, MPI_COMM_WORLD);
		}
		else 
		{
			MPI_Recv(&message, 1, MPI_INTEGER, rank - 1, 43, MPI_COMM_WORLD, &status);
			cout << "rank =  " << rank << "\tmessage = " << message << endl;
			message++;
			MPI_Send(&message, 1, MPI_INTEGER, rank + 1, 43, MPI_COMM_WORLD);
		}
	}

	if (rank == 0)
	{
		cout << "Message = " << message << endl;
	}

	MPI_Finalize();
	return 0;
}