#include "pch.h"
#include <iostream>
#include "mpi.h"

using namespace std;

void allReduce1(int *sendbuf, int *recvbuf, int count, MPI_Op op, MPI_Comm comm)
{
	int rank, size;

	MPI_Comm_size(comm, &size);
	MPI_Comm_rank(comm, &rank);

	for (int i = 0; i < count; i++)
	{
		recvbuf[i] = sendbuf[i];
	}

	MPI_Barrier(comm);

	MPI_Request* requests = new MPI_Request[size - 1];
	for (int i = 0, rcount = 0; i < size; i++) 
	{
		if (i == rank) 
		{
			continue;
		}

		MPI_Isend(sendbuf, count, MPI_INT, i, 0, comm, &requests[rcount++]);
	}

	MPI_Status status;
	int* received = new int[count];
	for (int i = 0; i < size - 1; i++) 
	{
		MPI_Recv(received, count, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, comm, &status);

		for (int j = 0; j < count; j++)
		{
			recvbuf[j] += received[j];
		}
	}

	delete[] received;

	MPI_Barrier(comm);
}

void allReduce2(int *sendbuf, int *recvbuf, int count, MPI_Op op, MPI_Comm comm)
{
	int rank, size;

	MPI_Comm_size(comm, &size);
	MPI_Comm_rank(comm, &rank);

	for (int i = 0; i < count; i++)
	{
		recvbuf[i] = sendbuf[i];
	}

	int middle = size;
	int* received = new int[count];
	MPI_Status status;

	MPI_Barrier(comm);

	while (middle > 1)
	{
		if (rank < middle / 2)
		{
			MPI_Recv(received, count, MPI_INT, middle - rank - 1, 0, comm, &status);

			for (int j = 0; j < count; j++)
			{
				recvbuf[j] += received[j];
			}
		}
		else if (rank < middle && rank != middle - rank - 1)
		{
			MPI_Send(recvbuf, count, MPI_INT, middle - rank - 1, 0, comm);
		}

		middle = middle / 2 + (middle % 2 ? 1 : 0);
	}

	MPI_Barrier(comm);

	MPI_Request* requests = new MPI_Request[size - 1];
	if (rank == 0)
	{
		for (int i = 1; i < size; i++)
		{
			MPI_Isend(recvbuf, count, MPI_INT, i, 0, comm, &requests[i]);
		}
		MPI_Waitall(size - 1, requests, MPI_STATUSES_IGNORE);
	}
	else
	{
		MPI_Recv(recvbuf, count, MPI_INT, 0, 0, comm, &status);
	}

	delete[] received;
	MPI_Barrier(comm);
}

double measureTimeMyAllreduce(int arraySize)
{
	int* in = new int[arraySize];
	int* out = new int[arraySize];

	for (int i = 0; i < arraySize; i++)
	{
		in[i] = i;
		out[i] = 0;
	}

	double startTime = MPI_Wtime();
	allReduce2(in, out, arraySize, MPI_SUM, MPI_COMM_WORLD);
	double endTime = MPI_Wtime();

	delete[] in;
	delete[] out;

	return endTime - startTime;
}

double measureTimeMPI_Allreduce(int arraySize)
{
	int* in = new int[arraySize];
	int* out = new int[arraySize];

	for (int i = 0; i < arraySize; i++)
	{
		in[i] = i;
		out[i] = 0;
	}

	double startTime = MPI_Wtime();
	MPI_Allreduce(in, out, arraySize, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	double endTime = MPI_Wtime();

	delete[] in;
	delete[] out;

	return endTime - startTime;
}

void myAllReduceTest()
{
	const int arraySize = 10;
	int* in = new int[arraySize];
	int* out = new int[arraySize];
	int rank, size;

	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	for (int i = 0; i < arraySize; i++)
	{
		in[i] = i;
		out[i] = 0;
	}

	allReduce2(in, out, arraySize, MPI_SUM, MPI_COMM_WORLD);

	cout << "rank = " << rank << " ->\t";
	for (int i = 0; i < arraySize; i++)
	{
		cout << out[i] << "\t";
	}
	cout << endl;
	fflush(stdout);

	delete[] in;
	delete[] out;
}

int main(int argc, char* argv[])
{
	const int arraySize = 10;
	const int repetitions = 10;
	double totalTime = 0.0;
	int rank, size;

	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	myAllReduceTest();

	for (int i = 0; i < repetitions; i++)
	{
		totalTime += measureTimeMyAllreduce(10);
	}

	cout << "rank = " << rank << "\taverage myAllreduce time = " << totalTime / repetitions << endl;

	totalTime = 0.0;
	for (int i = 0; i < repetitions; i++)
	{
		totalTime += measureTimeMPI_Allreduce(10);
	}

	cout << "rank = " << rank << "\taverage MPI_Allreduce time = " << totalTime / repetitions << endl;
	
	MPI_Finalize();
	return 0;
}