#include "pch.h"
#include <iostream>
#include "mpi.h"

using namespace std;

int main(int argc, char* argv[])
{
	int size, rank;
	const int n = 10000;
	double startTime, endTime;
	MPI_Request request, receiveRequest[n];
	MPI_Status statuses[n], receiveStatuses[n];
	MPI_Init(&argc, &argv);

	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	if (rank == 0)
	{
		char message[] = "message";
		startTime = MPI_Wtime();
		for (int i = 0; i < n; i++)
		{
			MPI_Isend(message, 7, MPI_CHAR, 1, i, MPI_COMM_WORLD, &request);
		}
		endTime = MPI_Wtime();
		cout << "rank: " << rank << "\ttime = " << (endTime - startTime) / n;
	}
	else if (rank == 1)
	{
		char receivedMessage[n][7];
		startTime = MPI_Wtime();
		for (int i = 0; i < n; i++)
		{
			MPI_Irecv(receivedMessage[i], 7, MPI_CHAR, 0, i, MPI_COMM_WORLD, &receiveRequest[i]);
		}
		MPI_Waitall(n, receiveRequest, receiveStatuses);
		endTime = MPI_Wtime();
		cout << "rank: " << rank << "\ttime = " << (endTime - startTime) / n;
	}

	MPI_Finalize();
	return 0;
}