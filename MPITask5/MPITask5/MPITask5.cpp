#include "pch.h"
#include <iostream>
#include "mpi.h"

using namespace std;

int main(int argc, char* argv[])
{
	int size, rank;
	const int n = 10000;
	double startTime, endTime;
	MPI_Init(&argc, &argv);

	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	if (rank == 0)
	{
		char message[] = "message"; 
		startTime = MPI_Wtime();
		for (int i = 0; i < n; i++)
		{
			MPI_Send(message, 7, MPI_CHAR, 1, 43, MPI_COMM_WORLD);
		}
		endTime = MPI_Wtime();
		cout << "rank: " << rank << "\ttime = " << (endTime - startTime) / n;
	} else if (rank == 1)
	{
		char receivedMessage[7];
		MPI_Status status;
		startTime = MPI_Wtime();
		for (int i = 0; i < n; i++)
		{
			MPI_Recv(receivedMessage, 7, MPI_CHAR, 0, 43, MPI_COMM_WORLD, &status);
		}
		endTime = MPI_Wtime();
		cout << "rank: " << rank << "\ttime = " << (endTime - startTime) / n;
	}

	MPI_Finalize();
	return 0;
}