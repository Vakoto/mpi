#include "pch.h"
#include <iostream>
#include "mpi.h"

using namespace std;

int main(int argc, char* argv[])
{
	int rank, size;
	MPI_Status status;

	MPI_Init(&argc, &argv);

	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	if (rank % 2 == 0)
	{
		if (rank + 1 < size)
		{
			MPI_Send(&rank, 1, MPI_INT, rank + 1, 43, MPI_COMM_WORLD);
		}
	}
	else
	{
		int receivedRank;
		MPI_Recv(&receivedRank, 1, MPI_INTEGER, rank - 1, 43, MPI_COMM_WORLD, &status);
		cout << "Process " << rank << ": " << receivedRank << " received from the process with rank " << (rank - 1) << endl; fflush(stdout);
	}

	MPI_Finalize();
	return 0;
}